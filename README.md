![screenshot](exampleSite/static/screenshot.png)

## Contents
+ [Usage](#usage)
+ [Features](#features)
+ [License](#license)

***

## Usage
`git clone https://gitlab.com/snow2k9/hugo-pointme.git themes/hugo-pointme --recursive`

## Features

+ Simple Map with markers
+ Built with [Hugo](https://gohugo.io), a hyperfast Golang generator
+ Just put your PoIs in a JSON File
+ Custom Marker


## License

[MIT](LICENSE.md) © Maximilian Bosche
