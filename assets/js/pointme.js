var map = L.map('map');
var customMarker;
var grp = [];

L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={{ $.Site.Params.MapBoxToken }}', {
  maxZoom: 18,
  attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
    '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
    'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
  id: 'mapbox.streets'
}).addTo(map);

{{ with $.Site.Params.CustomMarker }}

  customMarker = L.icon({
      iconUrl:          "{{ .iconurl | default "/images/marker-icon.png" }}",
      shadowUrl:        "{{ .shadowurl | default "/images/marker-shadow.png" }}",
      iconSize:         "{{ .iconsize | default ""}}".split(','),
      iconRetinaUrl:    "{{ .iconretinaurl | default ""}}", 
      shadowRetinaUrl:  "{{ .shadowretinaurl | default "" }}",
      shadowSize:       "{{ .shadowsize | default ""}}".split(','),
      iconAnchor:       "{{ .iconanchor | default ""}}".split(','),
      shadowAnchor:     "{{ .shadowanchor | default ""}}".split(','),
      popupAnchor:      "{{ .popupanchor | default ""}}".split(',')
  });

{{ end }}

{{ range $.Site.Data.points }}
  var popUpContent = "<strong>{{ .title }}</strong><p>{{ .content }}</p>";
  if (!customMarker) {
    grp.push(L.marker(["{{ .latitude }}", "{{ .longitude }}"])
      .bindPopup(popUpContent))
  } else {
    grp.push(L.marker(["{{ .latitude }}", "{{ .longitude }}"], {icon: customMarker})
      .bindPopup(popUpContent))
  }
{{ end }}

var features = L.featureGroup(grp).addTo(map)

map.fitBounds(features.getBounds())
